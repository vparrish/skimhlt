# SkimHLT

To be used for tau/egamma expert on call shifting. The txt files added in Jira tickets have many chains displayed in them. To make our lives a little bit easier, I have created a skimming script to remove non-monitored chains.

If you have changed the script in a way that is beneficial for the next shifter (i.e., updated the chain list), please push to the git repo.

# How to use?
Inside you will see a list of `triggers`. These contain the current trigger list (PLEASE DOUBLE CHECK THE NAMES ARE UP TO DATE) and skims the HLT txt files for only those triggers.

It will then create a csv file you can open anywhere that just shows you the triggers we monitor. Makes it a lot easier to manage that massive txt file!


Please check trigger names are correct at the beginning of your shift, and request to update this repo from me (Victoria) with the new names.
https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrigEgammaTauExpertShifterRun3

Happy shifting! 
