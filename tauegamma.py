# Written by Victoria A. Parrish
# May 10, 2022 
# To be used as skimming script for tau/egamma expert JIRA tickets
# To use: 
	# -> ensure the trigger list is up to date to chains that are meant to be monitored
	# -> download the HLTxxxx.txt file to same directory as where this python script is saved
	# -> run script, your skimmed file will be in this directory, named the HLTxxxx_skimmed.csv 
import argparse
import csv
import pandas as pd
import numpy as np

def main(args):
	print("Starting skimming")
	print("Warning:::::::: please check the twiki that the triggers are complete and accurate")
	# List checked latest as of May 16 2022
	triggers = {
		"HLT_e26_lhtight_ivarloose_L1EM22VHI",
		"HLT_e26_lhtight_ivarloose_L1eEM26M",
		"HLT_e60_lhmedium_L1EM22VHI",
		"HLT_e140_lhloose_L1EM22VHI",
		"HLT_g20_tight_icaloloose_L1EM15VHI",
		"HLT_g22_tight_L1EM15VHI",
		"HLT_g22_tight_L1eEM18M",
		"HLT_g140_loose_L1EM22VHI",
		"HLT_g140_loose_L1eEM26M,"

		"HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dRAB30_L1DR-TAU20ITAU12",J25	 
		"HLT_tau40_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA_03dRAB_L1TAU25IM_2TAU20IM_2J25_3J20",
		"HLT_tau40_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA_03dRAB_L1cTAU35M_2cTAU30M_2jJ55_3jJ50",
		"HLT_tau80_mediumRNN_tracktwoMVA_tau60_mediumRNN_tracktwoMVA_03dRAB_L1TAU60_2TAU40",
		"HLT_tau80_mediumRNN_tracktwoMVA_tau60_mediumRNN_tracktwoMVA_03dRAB_L1eTAU80_2eTAU60",
		"HLT_tau160_mediumRNN_tracktwoMVA_L1TAU100",
		"HLT_tau160_mediumRNN_tracktwoMVA_L1eTAU140",
		"HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_03dRAB_probe_L1EM22VHI",
		"HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_03dRAB_probe_L1eEM26M",
		"HLT_mu26_ivarmedium_tau20_mediumRNN_tracktwoMVA_03dRAB_probe_L1MU14FCH",
		"HLT_mu26_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_L1eTAU12_03dRAB_L1MU14FCH",
		"HLT_tau80_mediumRNN_tracktwoLLP_tau60_mediumRNN_tracktwoLLP_03dRAB_L1TAU60_2TAU40",
		"HLT_tau80_mediumRNN_tracktwoLLP_tau60_mediumRNN_tracktwoLLP_03dRAB_L1eTAU80_2eTAU60",

		"HLT_tau25_idperf_tracktwoMVA_L1TAU12IM",
		"HLT_tau25_perf_tracktwoMVA_L1TAU12IM",
		"HLT_tau25_mediumRNN_tracktwoMVA_L1TAU12IM",
		"HLT_tau25_idperf_tracktwoMVA_L1eTAU20",
		"HLT_tau25_perf_tracktwoMVA_L1eTAU20",
		"HLT_tau25_mediumRNN_tracktwoMVA_L1eTAU20",
		"HLT_tau25_mediumRNN_tracktwoMVA_L1eTAU20M",
		"HLT_tau25_mediumRNN_tracktwoMVA_L1cTAU20M",
	}
	file = args.inputFile
	df = pd.read_csv(file+".txt", skiprows=6,  delim_whitespace=True, header=None, names=range(4))
	df.to_csv("test.csv")
	colname = df.columns[0]
	dfnew = df.loc[df[colname].isin(triggers)]
	if args.NChains is not len(dfnew.index):
		print("STOP! Number of rows should be {}, the actual is: ".format(args.NChains), len(dfnew.index))
	else:
		print(dfnew)
		dfnew.to_csv("{}_skimmed.csv".format(file))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--inputFile", required=True, help="The name of your HLT chain file (please don't include the `.txt` end bit)")
    parser.add_argument("--iamsure", required=True, action="store_true", help="Did you double check that the chains are complete and correctly named?")
    parser.add_argument("-N", "--NChains", required=True, type=int, help="Number of chains montiored (check twiki")
    args = parser.parse_args()
    main(args)